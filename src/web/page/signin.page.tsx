import * as React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { SigninAction } from './signin.action'

export class SigninPage extends React.Component<any> {
  constructor(prop) {
    super(prop)
    this.state = {
      companyId: ''
    }
  }

  render() {
    return (
      <div className="signin">
        12345
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {

  }
}

export const SigninContainer = connect(mapStateToProps,
  (dispatch) => ({ actions: bindActionCreators(SigninAction.actions, dispatch) })
)(SigninPage)
