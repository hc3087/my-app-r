import { push } from 'react-router-redux'

export class SigninAction {
  public static actions = {
    goSignUp: () => {
      return async (dispatcher) => {
        dispatcher(
          push('/signup')
        )
      }
    },
    goSignIn: () => {
      return async (dispatcher) => {
        dispatcher(
          push('/signin')
        )
      }
    },

  }
}
