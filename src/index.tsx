import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
import { SigninContainer } from './web/page/signin.page'
import { store } from './initialData'


ReactDOM.render(
  <Provider store={store}>
      <Router>
          <Route exact={true} path="/" component={SigninContainer} />
      </Router>
  </Provider>
      ,

  document.getElementById('root')
)

