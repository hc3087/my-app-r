import * as React from 'react'
import { Component } from 'react'
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';
import '../styles/App.css';

export class App extends React.Component {
  render() {
    return (
        <Router>
            <div>
                <ul>
                    <li><Link to='/'>首页</Link></li>
                    <li><Link to='/about'>关于</Link></li>
                    <li><Link to='/topics'>主题列表</Link></li>
                </ul> 
                <hr/>
            </div>
        </Router>
    );
  }
}

export default App